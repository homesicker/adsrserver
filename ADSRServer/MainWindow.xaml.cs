﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Runtime.InteropServices;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Newtonsoft.Json.Linq;


namespace ADSRServer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const int PORT_NUMBER = 7001;
        private const int bufSize = 8 * 1024;
        public int clientCount = 0;

        private KeyEventArgs cl1k = null;
        private KeyEventArgs cl2k = null;

        const int VK_UP = 0x57;
        const int VK_RIGHT = 0x51;
        const uint KEYEVENTF_KEYUP = 0x0002;
        const uint KEYEVENTF_EXTENDEDKEY = 0x0001;

        private TcpListener tcpListener;
        //private List<TcpClient> tcpClients = new List<TcpClient>();
        private Thread t1 { get; set; }
        private Thread t3 { get; set; }
        private TcpClient handler { get; set; }
        public bool IsCanceled { get; set; }
        public IPAddress LocalIPAddress { get; set;}

        public MainWindow()
        {
            InitializeComponent();
            LocalIPAddress = GetLocalIPAddress();
            Console.WriteLine(LocalIPAddress);
            this.srvIP.Content = LocalIPAddress.ToString();
            IsCanceled = true;
            this.cl1_textkey.KeyDown += new KeyEventHandler(keyDownSet);
            //this.cl2_textkey.KeyDown += new KeyEventHandler(keyDownSet);
            this.btnTCPListenerToggler.Click += TCPListenerToggler;
        }

        public void TCPListenerToggler(object sender, RoutedEventArgs routedEventArgs)
        {
            if (IsCanceled)
            {
                //Console.WriteLine("IsCanseled true");
                this.btnTCPListenerToggler.Content = "Остановить сервер";
                StartScoketTCPListener();
                
            }
            else
            {
                handler.Client.Close();
                //handler = null;
                tcpListener.Stop();
                //tcpListener = null;
                t1.Abort();
                t3.Abort();
                t3.Abort();
                IsCanceled = true;
                this.btnTCPListenerToggler.Content = "Запустить сервер";
                clientCount = 0;
                this.Dispatcher.Invoke(() =>
                {
                    labelClientCount.Content = "0";
                });
            }
        }

        private void StartScoketTCPListener()
        {
            if (LocalIPAddress != null)
            {
                try
                {
                    tcpListener = new TcpListener(LocalIPAddress, PORT_NUMBER);
                    tcpListener.Start();
                    IsCanceled = false;
                    StartThread();
                    
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
                
            }
        }
        private void StartThread()
        {
            try
            {
                t1 = new Thread(new ParameterizedThreadStart(StartListener));
                t1.IsBackground = true;
                t1.Start();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private void StartListener(Object obj)
        {
            try
            {
                while (true)
                {
                    
                    handler = tcpListener.AcceptTcpClient();
                    if (handler.Connected)
                    {
                        clientCount++;
                        this.Dispatcher.Invoke(() =>
                        {
                            labelClientCount.Content = clientCount.ToString();
                        });                        
                    }
                    try
                    {
                        t3 = new Thread(() => { HandleDevice(handler); UpdateLabel(handler); }); 
                        t3.IsBackground = true;
                        t3.Start();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                    
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private void HandleDevice(Object obj)
        {
            TcpClient tcpClient = (TcpClient)obj;
            var stream = tcpClient.GetStream();
            Byte[] bytes = new Byte[bufSize];
            try
            {
                while (stream.Read(bytes, 0, bytes.Length) != 0)
                {
                    try
                    {
                        string[] data = Encoding.UTF8.GetString(bytes, 0, stream.Read(bytes, 0, bytes.Length)).Split(';');
                        dynamic json = JObject.Parse(data[0]);
                        float value = float.Parse(json.X.ToString()) + float.Parse(json.Y.ToString()) + float.Parse(json.Z.ToString());
                        //Console.WriteLine(json.Client.ToString());
                        if (json.Client.ToString() == "1")
                        {
                            this.Dispatcher.Invoke(() =>
                            {
                                pbValueClientOne.Value = (int)((value / 2) * sClientOne.Value);
                                int foo = (int)((value / 2) * sClientOne.Value);
                                if (foo > 50)
                                {
                                    Client1KeyDown(5);
                                }
                            });
                            
                        }
                        if (json.Client.ToString() == "2")
                        {
                            this.Dispatcher.Invoke(() =>
                            {
                                pbValueClientTwo.Value = (int)((value / 2) * sClientTwo.Value);
                                int foo = (int)((value / 2) * sClientTwo.Value);
                                if (foo > 50)
                                {
                                    Client2KeyDown(5);
                                }
                            });
                            
                            
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public void keyDownSet(object sender, KeyEventArgs e)
        {
            if (sender == cl1_textkey)
            {
                int ascii = KeyInterop.VirtualKeyFromKey(e.Key);
                cl1_textkey.Text = e.Key.ToString();
                Console.WriteLine(e.Key);
                cl1k = e;
            }
            if (sender == cl2_textkey)
            {
                int ascii = KeyInterop.VirtualKeyFromKey(e.Key);
                cl2_textkey.Text = e.Key.ToString();
                //Console.WriteLine(ascii);
                cl2k = e;
            }
        }

        [DllImport("user32.dll")]
        public static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, uint dwExtraInfo);
        public void Client1KeyDown(int count)
        {
            for (int i = 0; i < count; i++)
            {
                keybd_event((byte)VK_UP, 0, KEYEVENTF_EXTENDEDKEY | 0, 0);
                Thread.Sleep(10);
                keybd_event((byte)VK_UP, 0, KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP, 0);
            }
        }
        public void Client2KeyDown(int count)
        {
            for (int i = 0; i < count; i++)
            {
                keybd_event((byte)VK_RIGHT, 0, KEYEVENTF_EXTENDEDKEY | 0, 0);
                Thread.Sleep(10);
                keybd_event((byte)VK_RIGHT, 0, KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP, 0);
            }
        }
        public void UpdateLabel (TcpClient h)
        {
            clientCount--;
            this.Dispatcher.Invoke(() =>
            {
                labelClientCount.Content = clientCount.ToString();
            });
        }
        public static IPAddress GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip;
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }
    }
}
